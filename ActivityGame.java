package tomgodmon.stireborn;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ActivityGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture logo; //to be credit logo
	final float LOGO_WIDTH = 1500.0f; //Width of credit logo
	float logoHeight; //to be calculated once image is imported from assets
	
	@Override
	public void create () { //start makin' shtuff
		batch = new SpriteBatch();
		logo = new Texture("logo.png"); //import image from assets
		logoHeight = logo.getHeight() *LOGO_WIDTH / logo.getWidth(); //calculate height
	}

	@Override
	public void render () { //start drawing
		clearScreen(Color.BLACK); //Black background

		batch.begin(); //run program with image processing

		//Draw centered logo
		batch.draw(logo,
				(Gdx.graphics.getWidth() - LOGO_WIDTH) / 2,
				(Gdx.graphics.getHeight() - logoHeight) / 2,
				LOGO_WIDTH,
				logoHeight);

		batch.end();
	}

	//Takes color splits it to 4 parts and draws
	private void clearScreen(Color color) {
		Gdx.gl.glClearColor(color.r, color.g, color.b, color.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void dispose () {
		batch.dispose();
		logo.dispose();
	}
}
